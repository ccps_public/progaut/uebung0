cmake_minimum_required(VERSION 3.26)
project(Uebung0)

set(CMAKE_C_STANDARD 11)
set(CMAKE_C_STANDARD_REQUIRED ON)
set(CMAKE_CXX_STANDARD 14)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

add_executable(Uebung0
        src/main.c
        src/controller.c
        src/controller.h
)

# Fetch GoogleTest
Include(FetchContent)
FetchContent_Declare(
        googletest
        GIT_REPOSITORY https://github.com/google/googletest.git
        GIT_TAG        v1.15.2
)
set(gtest_force_shared_crt ON CACHE BOOL "" FORCE)
FetchContent_MakeAvailable(googletest)
# Build Tests
enable_testing()


add_executable(test_controller
        src/controller.c
        src/controller.h
        src/controller.test.cpp
)

target_link_libraries(test_controller PRIVATE GTest::gtest_main)
target_include_directories(test_controller PRIVATE src/)
include(GoogleTest)
gtest_discover_tests(test_controller)