#include <gtest/gtest.h>
extern "C" {
#include "controller.h"
}
TEST(ControllerTest, gain) {
    EXPECT_EQ(controller(8, 0, 500), 16);
    EXPECT_EQ(controller(-2, 0, 500), -4);
    EXPECT_EQ(controller(0, 2, 500), -4);
}

TEST(ControllerTest, saturation) {
    EXPECT_EQ(controller(2, 0, 2), 2);
    EXPECT_EQ(controller(-2, 0, 2), -2);
}